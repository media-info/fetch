import {AxiosRequestConfig} from 'axios';

export type HttpConfig = AxiosRequestConfig;

export type ConfigRequestType =
    | {
        requestInterval?: number;
        requestIntervalMax?: never;
        requestIntervalMin?: never;
    }
    | {
        requestInterval?: never;
        requestIntervalMax: number;
        requestIntervalMin: number;
    }

export type ConfigRetryType = {
    retry?: number;
} & ({
    retryInterval?: number;
    retryIntervalMax?: never;
    retryIntervalMin?: never;
} | {
    retryInterval?: never;
    retryIntervalMax: number;
    retryIntervalMin: number;
});

export interface ConfigBase {
    http?: HttpConfig;
    chunkSize: number;
    proxyUrl?: string;
    baseUrl: string;
}

export type Config = ConfigBase & ConfigRequestType & ConfigRetryType;