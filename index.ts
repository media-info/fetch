import axios, {AxiosRequestConfig} from 'axios';
import {Config, HttpConfig} from './types';

export * from './types';

export class Fetch {
    headers = {}

    httpConfig: HttpConfig = {
        headers: this.headers,
    };
    config: Config;
    private isFirstRequest = true;

    get retryInterval() {
        return this.config.retryInterval || this.retryRandomInterval;
    }

    get retryRandomInterval() {
        return Fetch._randomInt(this.config.retryIntervalMin as number, this.config.retryIntervalMax as number)
    }

    get requestInterval() {
        return this.config.requestInterval || this.requestRandomInterval;
    }

    get requestRandomInterval() {
        return Fetch._randomInt(this.config.requestIntervalMin as number, this.config.requestIntervalMax as number)
    }

    get requestHasInterval() {
        return this.config.requestInterval || this.config.requestIntervalMin;
    }

    constructor(config: Config) {
        this.config = {
            retry: 3,
            ...config,
        };
        if (!this.config.retryInterval && !this.config.retryIntervalMax) this.config.retryInterval = 1000;
        this.httpConfig = {
            ...config.http,
            headers: {
                ...this.httpConfig.headers,
                ...this.headers,
            }
        };
    }

    async req<T>(url: string, opt: AxiosRequestConfig = {}) {
        if (!this.isFirstRequest && this.requestHasInterval) {
            await new Promise(res => setTimeout(res, this.requestInterval));
        }
        this.isFirstRequest = false;
        if (url.substr(0, 1) != '/') url += '/';
        return this.retry(() => axios.get<T>((this.config.proxyUrl || this.config.baseUrl) + url, {...this.httpConfig, ...opt }), this.config.retry);
    }

    private async retry<T>(fn: () => Promise<T>, retries=0): Promise<T> {
        if (!retries) {
            return fn();
        }
        let err;
        while (retries > 0) {
            try {
                return await fn();
            } catch (e) {
                err = e;
                await new Promise((resolve) => {
                    setTimeout(resolve, this.retryInterval);
                });
            }
        }
        return Promise.reject(err);
    }

    private static _randomInt(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private promiseTimeout<T>(ms: number, promise: Promise<T>) {

        // Create a promise that rejects in <ms> milliseconds
        let timeout = new Promise<T>((resolve, reject) => {
            let id = setTimeout(() => {
                clearTimeout(id);
                reject('Timed out in ' + ms + 'ms.')
            }, ms)
        })

        // Returns a race between our timeout and the passed in promise
        return Promise.race([
            promise,
            timeout
        ])
    }

    // private promiseSleep<T>(promise: Promise<T>, min: number, max: number) {
    //     return new Promise(resolve => setTimeout(resolve, Math.floor(Math.random() * (max - min + 1) + min)))
    // }

    async chunkedPromise<T, K>(items: T[], cb: (i: T) => Promise<K>) {
        const data: K[] = [];
        const chunks = items.reduce<T[][]>((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / (this.config.chunkSize));

            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = [] // start a new chunk
            }

            resultArray[chunkIndex].push(item);

            return resultArray
        }, []);
        for (const chunk of chunks) {
            const result = await Promise.all(chunk.map(id => this.promiseTimeout<K>(1200000, cb(id))));
            data.push(...result.filter(v => !!v));
        }
        return data;
    };
}
